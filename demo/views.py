from django.shortcuts import render
from .models import CustomUser
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from .serializers import UserSerializer, UserSerializerWithToken
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib.auth.hashers import make_password
from rest_framework import status


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)

        serializer = UserSerializerWithToken(self.user).data
        for k, v in serializer.items():
            data[k] = v

        return data


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


# user registration
@api_view(['POST'])
def registerUser(request):
    data = request.data

    try:
        user = CustomUser.objects.create(
            first_name=data['firstName'],
            last_name=data['lastName'],
            username=data['email'],
            email=data['email'],
            address=data['address'],
            phone_number=data['phone_number'],
            password=make_password(data['password']))


        serializer = UserSerializerWithToken(user, many=False)
        return Response(serializer.data)
    except:
        message = {'detail': 'User with this email already exists'}
        return Response(message, status=status.HTTP_400_BAD_REQUEST)


# get all users info
@api_view(['GET'])
@permission_classes([IsAdminUser])
def getUsers(request):
    users = CustomUser.objects.all()
    serializer = UserSerializer(users, many=True)
    return Response(serializer.data)


# get user details (will display the info of that user whose token is provided)
@api_view(['GET'])
@permission_classes([IsAuthenticated]) 
def getUserAccount(request):
    user = request.user
    serializer = UserSerializer(user, many=False)
    return Response(serializer.data)


# update user details
@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateUserAccount(request):
    user = request.user
    serializer = UserSerializerWithToken(user, many=False)
    data = request.data

    user.first_name = data['firstName']
    user.last_name = data['lastName']
    user.username = data['email']
    user.email = data['email']
    user.address = data['address']
    user.phone_number = data['phone_number']

    if data['password'] != "":
        user.password = make_password(data['password'])

    user.save()
    return Response(serializer.data)
